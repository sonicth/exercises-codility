// You are given N counters, initially set to 0, and you have two possible operations on them:

// increase(X) − counter X is increased by 1,
// max counter − all counters are set to the maximum value of any counter.
// A non-empty array A of M integers is given. This array represents consecutive operations:

// if A[K] = X, such that 1 ≤ X ≤ N, then operation K is increase(X),
// if A[K] = N + 1 then operation K is max counter.
// For example, given integer N = 5 and array A such that:

//     A[0] = 3
//     A[1] = 4
//     A[2] = 4
//     A[3] = 6
//     A[4] = 1
//     A[5] = 4
//     A[6] = 4
// the values of the counters after each consecutive operation will be:

//     (0, 0, 1, 0, 0)
//     (0, 0, 1, 1, 0)
//     (0, 0, 1, 2, 0)
//     (2, 2, 2, 2, 2)
//     (3, 2, 2, 2, 2)
//     (3, 2, 2, 3, 2)
//     (3, 2, 2, 4, 2)
// The goal is to calculate the value of every counter after all operations.

// Write a function:

// vector<int> solution(int N, vector<int> &A);

// that, given an integer N and a non-empty array A consisting of M integers, returns a sequence of integers representing the values of the counters.

// Result array should be returned as a vector of integers.

// For example, given:

//     A[0] = 3
//     A[1] = 4
//     A[2] = 4
//     A[3] = 6
//     A[4] = 1
//     A[5] = 4
//     A[6] = 4
// the function should return [3, 2, 2, 4, 2], as explained above.

// Write an efficient algorithm for the following assumptions:

// N and M are integers within the range [1..100,000];
// each element of array A is an integer within the range [1..N + 1].


// vector<int> solution(int N, vector<int> &A) {
//     // write your code in C++14 (g++ 6.2.0)
    
//     // counters
//     vector<int> cs(N, 0);
//     auto cmax = 0;
//     for (auto op: A)
//     {
//         if (op > (N+1) || op < 0)
//         {
//             cerr << "op incorrect "<<op<<endl;
//             throw range_error("counter out of range");
//         }
        
//         if (op == N+1)
//         {
//             for (auto &c:cs)
//                 c = cmax;
//         } 
//         else
//         {
//             ++cs[op-1];
//             cmax = max(cmax, cs[op-1]);
//         }
//     }
    
//     return cs;
// }


// // more complex
// vector<int> solution(int N, vector<int> &A) {
//     // write your code in C++14 (g++ 6.2.0)
    
//     // counters
//     vector<int> cs(N, 0);
//     auto cmax = 0;
    
//     // precompute couts
//     for (auto op: A)
//     {
//         if (op > (N+1) || op < 0)
//         {
//             cerr << "op incorrect "<<op<<endl;
//             throw range_error("counter out of range");
//         }
        
//         if (op != N+1)
//             ++cs[op-1];   
//     }
    
    
//     // convert into map
//     int i = 0;
//     for (auto &c: cs)
//     {
//         if (c != 0)
//         {
//             c = i;       
//             ++i;
//         }
//         else
//         {
//             c = -1;
//         }
//     }
    
//     vector<int> csmap(i, 0);
     
//     auto localmax = 0;
//     for (auto op_wide: A)
//     {   
//         auto op = cs[op_wide-1];
        
//         if (op_wide == N+1)
//         {
//             localmax = cmax;
//             for (auto &cm:csmap)
//                 cm = cmax;
//         } 
//         else
//         {
//             ++csmap[op];
//             cmax = max(cmax, csmap[op]);
//         }
//     }
    
//     // copy back the values
//     for (auto &c: cs)
//     {
//         if (c == -1)
//             c = localmax;
//         else
//         {
//             auto idx = c;
//             c = csmap[idx];
//         }
//     }
    
//     return cs;
// }




// #include <algorithm>

// void dbCounts(auto &as)
// {
//     cerr << "**counts [";
//     for (auto &a:as)
//     {
//         auto is_last = &a == &as.back();
//         cerr << a << (is_last ? "" : ", ");
//     }
//     cerr << "]\n";
// }

// vector<int> solution(int N, vector<int> &A) {
//     // write your code in C++14 (g++ 6.2.0)
    
//     //1. find last index of N+1
//     int last_idx = N-1;
//     while (A[last_idx] != N+1 && last_idx > 0)
//         --last_idx;
    
//     //2. count maxes separated by N+1's
//     int i = 0;
//     vector<int> counts(N);
//     auto total_max = 0;
//     auto M = (int) A.size();
//     while (i < M)
//     {
        
//         // get counts
//         while (A[i] != N+1 && i < M)
//         {
//             ++counts[ A[i]-1 ];
//             ++i;
//         }
        
//         // unless we are at the position of last N+1, sum total
//         if (i <= last_idx)
//         {
//             // get max element
//             auto local_max = *max_element(begin(counts), end(counts));            

//             // sum up max
//             total_max += local_max;
            
//             if (i != last_idx)
//             // clear the contents
//             fill(begin(counts), end(counts), 0);
//         }
        
//         // skip over N+1
//         ++i;
//     }
    
//     // add total_max to counts
//     for (auto &c: counts)
//     {
//         c += total_max;
//     }
    
//     return counts;
// }


// You are given N counters, initially set to 0, and you have two possible operations on them:

// increase(X) - counter X is increased by 1,
// max counter - all counters are set to the maximum value of any counter.
// A non-empty array A of M integers is given. This array represents consecutive operations:

// if A[K] = X, such that 1 ≤ X ≤ N, then operation K is increase(X),
// if A[K] = N + 1 then operation K is max counter.
// For example, given integer N = 5 and array A such that:

//     A[0] = 3
//     A[1] = 4
//     A[2] = 4
//     A[3] = 6
//     A[4] = 1
//     A[5] = 4
//     A[6] = 4
// the values of the counters after each consecutive operation will be:

//     (0, 0, 1, 0, 0)
//     (0, 0, 1, 1, 0)
//     (0, 0, 1, 2, 0)
//     (2, 2, 2, 2, 2)
//     (3, 2, 2, 2, 2)
//     (3, 2, 2, 3, 2)
//     (3, 2, 2, 4, 2)
// The goal is to calculate the value of every counter after all operations.

// Write a function:

// vector<int> solution(int N, vector<int> &A);

// that, given an integer N and a non-empty array A consisting of M integers, returns a sequence of integers representing the values of the counters.

// Result array should be returned as a vector of integers.

// For example, given:

//     A[0] = 3
//     A[1] = 4
//     A[2] = 4
//     A[3] = 6
//     A[4] = 1
//     A[5] = 4
//     A[6] = 4
// the function should return [3, 2, 2, 4, 2], as explained above.

// Write an efficient algorithm for the following assumptions:

// N and M are integers within the range [1..100,000];
// each element of array A is an integer within the range [1..N + 1].


// vector<int> solution(int N, vector<int> &A) {
//     // write your code in C++14 (g++ 6.2.0)
    
//     // counters
//     vector<int> cs(N, 0);
//     auto cmax = 0;
//     for (auto op: A)
//     {
//         if (op > (N+1) || op < 0)
//         {
//             cerr << "op incorrect "<<op<<endl;
//             throw range_error("counter out of range");
//         }
        
//         if (op == N+1)
//         {
//             for (auto &c:cs)
//                 c = cmax;
//         } 
//         else
//         {
//             ++cs[op-1];
//             cmax = max(cmax, cs[op-1]);
//         }
//     }
    
//     return cs;
// }


// // more complex
// vector<int> solution(int N, vector<int> &A) {
//     // write your code in C++14 (g++ 6.2.0)
    
//     // counters
//     vector<int> cs(N, 0);
//     auto cmax = 0;
    
//     // precompute couts
//     for (auto op: A)
//     {
//         if (op > (N+1) || op < 0)
//         {
//             cerr << "op incorrect "<<op<<endl;
//             throw range_error("counter out of range");
//         }
        
//         if (op != N+1)
//             ++cs[op-1];   
//     }
    
    
//     // convert into map
//     int i = 0;
//     for (auto &c: cs)
//     {
//         if (c != 0)
//         {
//             c = i;       
//             ++i;
//         }
//         else
//         {
//             c = -1;
//         }
//     }
    
//     vector<int> csmap(i, 0);
     
//     auto localmax = 0;
//     for (auto op_wide: A)
//     {   
//         auto op = cs[op_wide-1];
        
//         if (op_wide == N+1)
//         {
//             localmax = cmax;
//             for (auto &cm:csmap)
//                 cm = cmax;
//         } 
//         else
//         {
//             ++csmap[op];
//             cmax = max(cmax, csmap[op]);
//         }
//     }
    
//     // copy back the values
//     for (auto &c: cs)
//     {
//         if (c == -1)
//             c = localmax;
//         else
//         {
//             auto idx = c;
//             c = csmap[idx];
//         }
//     }
    
//     return cs;
// }





// You are given N counters, initially set to 0, and you have two possible operations on them:

// increase(X) - counter X is increased by 1,
// max counter - all counters are set to the maximum value of any counter.
// A non-empty array A of M integers is given. This array represents consecutive operations:

// if A[K] = X, such that 1 ≤ X ≤ N, then operation K is increase(X),
// if A[K] = N + 1 then operation K is max counter.
// For example, given integer N = 5 and array A such that:

//     A[0] = 3
//     A[1] = 4
//     A[2] = 4
//     A[3] = 6
//     A[4] = 1
//     A[5] = 4
//     A[6] = 4
// the values of the counters after each consecutive operation will be:

//     (0, 0, 1, 0, 0)
//     (0, 0, 1, 1, 0)
//     (0, 0, 1, 2, 0)
//     (2, 2, 2, 2, 2)
//     (3, 2, 2, 2, 2)
//     (3, 2, 2, 3, 2)
//     (3, 2, 2, 4, 2)
// The goal is to calculate the value of every counter after all operations.

// Write a function:

// vector<int> solution(int N, vector<int> &A);

// that, given an integer N and a non-empty array A consisting of M integers, returns a sequence of integers representing the values of the counters.

// Result array should be returned as a vector of integers.

// For example, given:

//     A[0] = 3
//     A[1] = 4
//     A[2] = 4
//     A[3] = 6
//     A[4] = 1
//     A[5] = 4
//     A[6] = 4
// the function should return [3, 2, 2, 4, 2], as explained above.

// Write an efficient algorithm for the following assumptions:

// N and M are integers within the range [1..100,000];
// each element of array A is an integer within the range [1..N + 1].


// vector<int> solution(int N, vector<int> &A) {
//     // write your code in C++14 (g++ 6.2.0)
    
//     // counters
//     vector<int> cs(N, 0);
//     auto cmax = 0;
//     for (auto op: A)
//     {
//         if (op > (N+1) || op < 0)
//         {
//             cerr << "op incorrect "<<op<<endl;
//             throw range_error("counter out of range");
//         }
        
//         if (op == N+1)
//         {
//             for (auto &c:cs)
//                 c = cmax;
//         } 
//         else
//         {
//             ++cs[op-1];
//             cmax = max(cmax, cs[op-1]);
//         }
//     }
    
//     return cs;
// }


// // more complex
// vector<int> solution(int N, vector<int> &A) {
//     // write your code in C++14 (g++ 6.2.0)
    
//     // counters
//     vector<int> cs(N, 0);
//     auto cmax = 0;
    
//     // precompute couts
//     for (auto op: A)
//     {
//         if (op > (N+1) || op < 0)
//         {
//             cerr << "op incorrect "<<op<<endl;
//             throw range_error("counter out of range");
//         }
        
//         if (op != N+1)
//             ++cs[op-1];   
//     }
    
    
//     // convert into map
//     int i = 0;
//     for (auto &c: cs)
//     {
//         if (c != 0)
//         {
//             c = i;       
//             ++i;
//         }
//         else
//         {
//             c = -1;
//         }
//     }
    
//     vector<int> csmap(i, 0);
     
//     auto localmax = 0;
//     for (auto op_wide: A)
//     {   
//         auto op = cs[op_wide-1];
        
//         if (op_wide == N+1)
//         {
//             localmax = cmax;
//             for (auto &cm:csmap)
//                 cm = cmax;
//         } 
//         else
//         {
//             ++csmap[op];
//             cmax = max(cmax, csmap[op]);
//         }
//     }
    
//     // copy back the values
//     for (auto &c: cs)
//     {
//         if (c == -1)
//             c = localmax;
//         else
//         {
//             auto idx = c;
//             c = csmap[idx];
//         }
//     }
    
//     return cs;
// }


// #include <algorithm>

// void dbCounts(auto &as)
// {
//     cerr << "**counts [";
//     for (auto &a:as)
//     {
//         auto is_last = &a == &as.back();
//         cerr << a << (is_last ? "" : ", ");
//     }
//     cerr << "]\n";
// }

// vector<int> solution(int N, vector<int> &A) {
//     // write your code in C++14 (g++ 6.2.0)
    
//     //1. find last index of N+1
//     int last_idx = N-1;
//     while (A[last_idx] != N+1 && last_idx > 0)
//         --last_idx;
    
//     //2. count maxes separated by N+1's
//     int i = 0;
//     vector<int> counts(N);
//     fill(begin(counts), end(counts), 0);
//     auto total_max = 0;
//     auto M = (int) A.size();
//     while (i < last_idx)
//     {
        
//         // get counts
//         while (A[i] != N+1 && i < M)
//         {
//             ++counts[ A[i]-1 ];
//             ++i;
//         }

//         auto local_max = *max_element(begin(counts), end(counts));  
//                     // get max element
//             // sum up max
//         total_max += local_max;
//         // unless we are at the position of last N+1, sum total
//          fill(begin(counts), end(counts), 0);
           
        
//         // skip over N+1
//         ++i;
//     }
    
//  //   dbCounts(counts);
//     while (i < M)
//     {
//         ++counts[ A[i]-1 ];
//         ++i;
//     }
    
//   //  dbCounts(counts);
//     // add total_max to counts
//     for (auto &c: counts)
//     {
//         c += total_max;
//     }
    
//   //  dbCounts(counts);
//     return counts;
// }



// #include <algorithm>

// void dbCounts(auto &as)
// {
//     cerr << "**counts [";
//     for (auto &a:as)
//     {
//         auto is_last = &a == &as.back();
//         cerr << a << (is_last ? "" : ", ");
//     }
//     cerr << "]\n";
// }


// class Counters
// {
//     vector<int> cs;
// public:
//     Counters(size_t n): cs(n)
//     {
//         clear();    
//     }
    
//     void clear()
//     {
//         fill(begin(cs), end(cs), 0);
//     }
    
//     void count(vector<int> const &as, int from, int to_notincluding)
//     {
//         while (from < to_notincluding)
//         {
//             ++cs[ as[from]-1 ];
//             ++from;
//         }
//     }
    
//     int getMaxCount() const 
//     {
//         return *max_element(begin(cs), end(cs));
//     }
    
//     void add(int value)
//     {
//         for (auto &c:cs)
//         {
//             c += value;
//         }
//     }
    
//     vector<int> && get()
//     {
//         return std::move(cs);
//     }
// };

// vector<int> getMaxIdxs(vector<int> const &as, int N)
// {
//     vector<int> idxs;
//     auto m = (int) as.size();
//     for (int i = 0; i < m; ++i)
//     {
//         if (as[i] == N+1)
//             idxs.push_back(i);
//     }
    
//     return idxs;
// }

// vector<int> solution(int N, vector<int> &A) {
//     // write your code in C++14 (g++ 6.2.0)
    
//     //1. find last index of N+1
//     auto max_idxs = getMaxIdxs(A, N);
    
//     //2. count maxes separated by N+1's

//     auto total_max = 0;
    
//     Counters cts(N);

//     // 1. count all max-sets, if any
//     int start_idx = 0;
//     for (auto max_idx: max_idxs)
//     {
//         cts.clear();
//         cts.count(A, start_idx, max_idx);
//         total_max += cts.getMaxCount();    
//         // update next idx!
//         start_idx = max_idx + 1;
        
//     }
    
//     // 2. count everything past max-sets
//     cts.clear();
//     cts.count(A, start_idx, A.size());
//     cts.add(total_max);
//     return cts.get();
// }



#include <algorithm>

void dbvec(auto &as, string name = "vector")
{
    return;
    cerr << "**"<<name<<" [";
    for (auto &a:as)
    {
        auto is_last = &a == &as.back();
        cerr << a << (is_last ? "" : ", ");
    }
    cerr << "]\n";
}


class Counters
{
    int const n;
    int const m;
    vector<int> cs; ///< counts
    vector<int> ccs; ///< compressed cs
    vector<int> &as; ///< data stream
    // vector<int> cas; ///< compressed as
    vector<int> max_idxs;

    void compress()
    {
        // clear 
        clearCs();
        
        // 1. a) build maxes b) build counts
        for (int i = 0; i < m; ++i)
        {
            // add max boundary indices
            if (as[i] == n+1)
                max_idxs.push_back(i);
            // at the same time fill counts
            else
                ++cs[ as[i]-1 ];
                
        }
        
        dbvec(cs, "cs counts");
        
        // 2. build map
        // convert existing count!
        //NOTE!!! starts with1!
        int i = 0;
        for (auto &c: cs)
        {
            if (c != 0)
            {
                c = i+1;       
                ++i;
            }
        }
        dbvec(cs, "cs map");
        
        // allocate compressed counters and set to zero!
        ccs.resize(i);
        clear();
        
        // 3. actually compress!
        for (int i = 0; i < m; ++i)
        {
            auto uncompressed_number = as[i];
            auto compressed_mumber = cs[ uncompressed_number-1 ];
            // NOTE normalize to zero!
            // cas[i] = compressed_mumber-1;
            as[i] = compressed_mumber-1;
        }
        
        
        dbvec(cs, "compressed as");
        
        // now we are ready to perform operations on compressed data
    }
public:
    Counters(size_t n_, vector<int> &as_)
        : n((int)n_), m((int) as_.size())
        , cs(n)
        , as(as_) // input data
        // , cas(m) // compressed as
    {
        compress();   
    }
    
    void clearCs()
    {
        fill(begin(cs), end(cs), 0);    
    }
    
    void clear()
    {
        fill(begin(ccs), end(ccs), 0);
    }
    
    void count(int from, int to_notincluding)
    {
        while (from < to_notincluding)
        {
            // ++cs[ as[from]-1 ];
            ++ccs[ as[from] ];
            ++from;
        }
        
        
        // dbvec(ccs, "compressed counts");
    }
    
    vector<int> const & getMaxIdxs() const
    {
        return max_idxs;   
    }
    
    int getMaxCount() const 
    {
        return *max_element(begin(ccs), end(ccs));
    }
    
    // void add(int value)
    // {
    //     for (auto &c:cs)
    //     {
    //         c += value;
    //     }
    // }
    
    vector<int> get(int max_count)
    {
        
        dbvec(ccs, "last counts");
        dbvec(cs, "uncompress map (before counts)");
        
        // restore values
        int j = 0;
        for (auto &c: cs)
        {
            if (c == 0)
                c = max_count;
            else
            {
                // de-index c
                c = ccs[c-1] + max_count;
                ++j;
            }
            
            
        }
        dbvec(cs, "final un-comprssed counts");
        
        // 
        // return std::move(cs);
        return (cs);
    }
};



vector<int> solution(int N, vector<int> &A) {
    // write your code in C++14 (g++ 6.2.0)
    

    
    //2. count maxes separated by N+1's

    auto total_max = 0;
    
    Counters cts(N, A);
    // cerr << "*step1\n";

    // 1. count all max-sets, if any
    int start_idx = 0;
    for (auto max_idx: cts.getMaxIdxs())
    {
        cts.clear();
        // cerr << "*step2a\n";
        cts.count(start_idx, max_idx);
        // cerr << "*step2b\n";
        total_max += cts.getMaxCount();    
        // update next idx!
        start_idx = max_idx + 1;
// cerr << "*step2e\n";        
    }
    
    // 2. count everything past max-sets
    cts.clear();
    cts.count(start_idx, A.size());
    // cerr << "*step3\n";
    return cts.get(total_max);
}

 
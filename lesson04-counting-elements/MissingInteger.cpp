// This is a demo task.

// Write a function:

// int solution(vector<int> &A);

// that, given an array A of N integers, returns the smallest positive integer (greater than 0) that does not occur in A.

// For example, given A = [1, 3, 6, 4, 1, 2], the function should return 5.

// Given A = [1, 2, 3], the function should return 4.

// Given A = [−1, −3], the function should return 1.

// Write an efficient algorithm for the following assumptions:

// N is an integer within the range [1..100,000];
// each element of array A is an integer within the range [−1,000,000..1,000,000].

// tests
//  -1, 1, 2, 3 
//  [-1000000, 1000000]

#include <algorithm>
#include <cassert>

int solution(vector<int> &A) {
    // write your code in C++14 (g++ 6.2.0)
    auto max_el = *max_element(begin(A), end(A));
    
    assert(max_el <= 1000'000);
    assert(A.size() <= 100'000);
    
    // no positive numbers, no need for array, return 1
    if (max_el < 1)
        return 1;
        
    vector<int> C(max_el, 0);
    
    for (auto a: A)
    {
        if (a > 0)
            ++C[a-1];
    }
    
    auto lowest = 1;
    auto pos = begin(C);
    while(*pos != 0 && pos != end(C))
    {
        ++lowest;
        ++pos;
    }
    
    return lowest;   
}
